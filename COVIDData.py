

from flask import Flask, jsonify,request
import json
import requests 


app = Flask(__name__)


def get(url):
    try:
        res = requests.get(url)
        return res.json()        
    except:
        return "cant connect to the api"

@ app.route('/newCasesPeak', methods=['GET'])
#newCasesPeak?country=israel
def get_newCasesPeak():
    country=request.args.get('country')
    r = requests.get("https://disease.sh/v3/covid-19/historical/{}".format(country))
    data=r.json()
    for key, value in data.items(): 
        if value == "Country not found or doesn't have any historical data":
            return jsonify()
        else:    
            dict1=(data["timeline"])
            dict2=(dict1["cases"])
            casemaxdate = max(dict2, key=dict2.get) 
            casemaxvalue = dict2.get(max(dict2, key=dict2.get))   
            return jsonify(
            country=country,
            method="newCasesPeak",
            date=casemaxdate,
            value=casemaxvalue)

@ app.route('/recoveredPeak', methods=['GET'])
#recoveredPeak?country=israel
def get_recoveredPeak():
    country=request.args.get('country')
    r = requests.get("https://disease.sh/v3/covid-19/historical/{}".format(country))
    data=r.json()
    
    for key, value in data.items(): 
        if value == "Country not found or doesn't have any historical data":
            return jsonify()
        else:
            dict1=(data["timeline"])
            dict2=(dict1["recovered"])
            casemaxdate = max(dict2, key=dict2.get) 
            casemaxvalue = dict2.get(max(dict2, key=dict2.get))   
            return jsonify(
            country=country,
            method="recoveredPeak",
            date=casemaxdate,
            value=casemaxvalue)

@ app.route('/deathsPeak', methods=['GET'])
#deathsPeak?country=israel
def get_deathsPeak():
    country=request.args.get('country')
    r = requests.get("https://disease.sh/v3/covid-19/historical/{}".format(country))
    data=r.json()
    for key, value in data.items(): 
        if value == "Country not found or doesn't have any historical data":
            return jsonify()
        else:
            dict1=(data["timeline"])
            dict2=(dict1["deaths"])
            casemaxdate = max(dict2, key=dict2.get) 
            casemaxvalue = dict2.get(max(dict2, key=dict2.get))  
            return jsonify(
            country=country,
            method="deathsPeak",
            date=casemaxdate,
            value=casemaxvalue)  



@ app.route('/status', methods=['GET'])
def get_status():
    test=get("https://disease.sh/v3/covid-19/historical/israel")
    if test == "cant connect to the api":
        return jsonify(status="faild")
    else:
        return jsonify(status="success")            

@ app.route('/', methods=['GET'])
def get_error():
    return jsonify()

@app.errorhandler(404)
def page_not_found(error):
    return jsonify()

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)



