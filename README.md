# COVID-Data 
The app gives information on which day during the last 30 days were the highest number of infected, dead, and recovering from the corona virus by using an appropriate API request respectively.

# pay attention

Make sure that Python 3.X and flask(webframework) are installed on your computer and your operating system is Linux Ubuntu Distribution.

# Getting started

First, clone this repository by the following command:

```bash
git clone https://gitlab.com/liel369o/fyber-test.git
```

Next, Move to the folder named fyber-test by the following command:

```bash
cd fyber-test
```
Now, To launch the application run the following command:

```bash
python3 COVIDData.py
```
# More info

The application serve 4 endpoints (examples can be seen below):

    ○ newCasesPeak - Returns the date (and value) of the highest peak of new
    Covid-19 cases in the last 30 days for a required country.

    ○ recoveredPeak - Returns the date (and value) of the highest peak of recovered
    Covid-19 cases in the last 30 days for the required country.

    ○ deathsPeak - Returns the date (and value) of the highest peak of death Covid-19
    cases in the last 30 days for a required country.

    ○ status - Returns a value of success / fail to contact the backend API

1.newCasesPeak
request:

```bash
curl localhost:5000/newCasesPeak?country=israel
```

| parameter     | type          | example | method         | 
| ------------- |:-------------:| -----:  | -----------:   |
| country       | String        | israel  | newCasesPeak   |

response

```bash
{"country":"israel","date":"9/29/20","method":"newCasesPeak","value":236926}
```


2.recoveredPeak
request:

```bash
curl localhost:5000/recoveredPeak?country=israel
```

| parameter     | type          | example | method         | 
| ------------- |:-------------:| -----:  | -----------:   |
| country       | String        | israel  | recoveredPeak  |

response

```bash
{"country":"israel","date":"9/29/20","method":"recoveredPeak","value":167804}
```


