FROM python:3.6.9
RUN apt-get update -y
RUN pip install --upgrade pip setuptools
RUN pip install requests
RUN pip3 install flask
RUN export FLASK_APP=COVIDData.py
RUN export FLASK_ENV=development
COPY . /
ENTRYPOINT python3 COVIDData.py
